/*John Stevenson
CSE 223
4/23/2020
*/



/*An object of class Even is an even integer 
Methods of this class include add (adding the Even to another Even),
sub (subtracting an Even from the Even), mul (multiplying the Even by another Even),
and div (dividing the Even by antoher Even). There are a few other methods such as 
toInt (returns the value of the Even) and toString (returns the Even as a string of 
the Even in the English equivalences).  
*/
public class Even{ 
	private int val; //val is the numerical value assigned to the Even
			 //val cannot be accessed outside of an object

	//This is the constructor for the Even class
	//If the number passed to the constructor is not even,
	//the Even will be given a value of one greater than 
	//the value it was passed
	public Even(int num) 
	{
		if(num%2!=0) num+=1; //if the value passed to the constructor is not even, the number will be incremented by one
		val=num; //assignment of the value of the Even
	}
	
	//The add method adds the value of another Even to the 
	//value of the Even used to call the method and returns 
	//an Even whose value is equal to the value calculated.
	public Even add(Even num)
	{
		return (new Even(val+num.toInt()));
	}

	//The sub method subtracts the value of another Even 
	//from the value of the Even used to call the method
	//and returns an Even whose value is equal to the 
	//value calculated
	public Even sub(Even num)
	{
		return (new Even(val-num.toInt()));
	}
		
	//The mul method multiplies the value of an Even by the 
	//value of the Even used to call the method and returns 
	//another Even whose value is equal to the value 
	//calculated
	public Even mul(Even num)
	{
		return (new Even(val*num.toInt()));
	}
	
	//The div method divides the value of the Even used 
	//to call the method by the value of another method
	//and returns an Even whose value is equal to the 
	//value calculated. No handling of dividing by zero 
	public Even div(Even num)
	{
		return (new Even(val/num.toInt()));
	}

	//The toInt method is used to access the value of 
	//an Even
	public int toInt()
	{
		return (val);
	}

	//The toString method parses the value of an Even 
	//into a string containing the English equivalent 
	//of every digit in the Even
	public String toString()
	{
		String temp=Integer.toString(val); //changes the Even value into a string to work with 
		int i=0; //i is the counter used as an index counter to access the different elements contained in the temp string created above
		String string=""; //string is used to store the output string 
		while(i<temp.length()) //loops through the entirety of temp, character by character
		{
			switch(temp.charAt(i)) //check the character at index i of temp, change it to the corresponding English word, and append it to the output string 
			{
				case '-': string=string.concat("negative ");
					break;
				case '0': string=string.concat("zero ");
					break;
				case '1': string=string.concat("one ");
					break;
				case '2': string=string.concat("two ");
					break;
				case '3': string=string.concat("three ");
					break;
				case '4': string=string.concat("four ");
					break;
				case '5': string=string.concat("five ");
					break;
				case '6': string=string.concat("six ");
					break;
				case '7': string=string.concat("seven ");
					break;
				case '8': string=string.concat("eight ");
					break;
				case '9': string=string.concat("nine ");
					break;
				default:
					break;
			}
			i++; //increment the counter to access the next character in the temp string
		}
		return (string); //return the output string
	}
}	
	
